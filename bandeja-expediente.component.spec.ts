import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BandejaExpedienteComponent } from './bandeja-expediente.component';

describe('BandejaExpedienteComponent', () => {
  let component: BandejaExpedienteComponent;
  let fixture: ComponentFixture<BandejaExpedienteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BandejaExpedienteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BandejaExpedienteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
