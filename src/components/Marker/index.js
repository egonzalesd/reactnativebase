import React, { Component, Fragment } from "react";
import { View, Image ,StyleSheet,Text} from "react-native";
import MapView, { Marker } from "react-native-maps";

import Search from "../Search";

import marketImage from '../../assets/marker.png';


function getRandomInt(min, max) {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min)) + min;
}


export default class MarkerPoint extends Component {

  constructor(props) {
    super(props);

    this.state = {
      region: null,
      markers: []
    }
    this.handlePress = this.handlePress.bind(this);
  }

  async componentDidMount() {
    navigator.geolocation.getCurrentPosition(
      async ({ coords: { latitude, longitude } }) => {

        this.setState({
          region: {
            latitude:-12.1769765,
            longitude:-76.9982209,
            latitudeDelta: 0.0143,
            longitudeDelta: 0.0134
          }
        });
      }, //sucesso
      () => {}, //erro
      {
        timeout: 2000,
        enableHighAccuracy: true,
        maximumAge: 1000
      }
    );
  }

  handlePress(e) {
    this.setState({
      markers: [
        ...this.state.markers,
        {
          coordinate: e.nativeEvent.coordinate,
          cost: `$${getRandomInt(50, 300)}`,
          key: 'key'+ `$${getRandomInt(50, 300)}`
        }
      ]
    })
  }

  handleBack = () => {
    this.setState({ destination: null });
  };

  render() {
    const { region} = this.state;

    return (
      <View style={{ flex: 1 }}>
        <MapView
          style={{ flex: 1 }}
          region={region}
          showsUserLocation
          loadingEnabled
          ref = {el => this.mapView = el }
          onPress={this.handlePress}
        >
        {this.state.markers.map((marker) => {
            return (
            <Marker {...marker} key={marker.key}>
                <View style={styles.marker}>
                <Text style={styles.text}>{marker.cost}</Text>
                </View>
            </Marker>
            )
        })}
        </MapView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
    },
    marker: {
      backgroundColor: "#550bbc",
      padding: 5,
      borderRadius: 5,
    },
    text: {
      color: "#FFF",
      fontWeight: "bold"
    }
  });